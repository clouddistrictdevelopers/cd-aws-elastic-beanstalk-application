## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_elastic_beanstalk_application.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/elastic_beanstalk_application) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_appversion_lifecycle_delete_source_from_s3"></a> [appversion\_lifecycle\_delete\_source\_from\_s3](#input\_appversion\_lifecycle\_delete\_source\_from\_s3) | Whether to delete application versions from S3 source | `bool` | `false` | no |
| <a name="input_appversion_lifecycle_max_count"></a> [appversion\_lifecycle\_max\_count](#input\_appversion\_lifecycle\_max\_count) | The max number of application versions to keep | `number` | `100` | no |
| <a name="input_appversion_lifecycle_service_role"></a> [appversion\_lifecycle\_service\_role](#input\_appversion\_lifecycle\_service\_role) | The ARN of an IAM service role under which the application version is deleted. If left empty, the `appversion_lifecycle` block will not be created | `string` | `""` | no |
| <a name="input_description"></a> [description](#input\_description) | Short description of the application | `string` | `""` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment where app should be deployed like dev, acc or prd. | `string` | `"dev"` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the application | `string` | `""` | no |
| <a name="input_project"></a> [project](#input\_project) | Name of project your app belongs to. | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_elastic_beanstalk_application_arn"></a> [elastic\_beanstalk\_application\_arn](#output\_elastic\_beanstalk\_application\_arn) | The ARN assigned by AWS for this Elastic Beanstalk Application |
| <a name="output_elastic_beanstalk_application_name"></a> [elastic\_beanstalk\_application\_name](#output\_elastic\_beanstalk\_application\_name) | Elastic Beanstalk Application name |
