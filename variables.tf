variable "name" {
  type        = string
  description = "The name of the application"
  default     = ""
}

variable "description" {
  type        = string
  description = "Short description of the application"
  default     = ""
}

variable "appversion_lifecycle_service_role" {
  type        = string
  description = "The ARN of an IAM service role under which the application version is deleted. If left empty, the `appversion_lifecycle` block will not be created"
  default     = ""
}

variable "appversion_lifecycle_max_count" {
  type        = number
  description = "The max number of application versions to keep"
  default     = 100
}

# variable "appversion_lifecycle_max_age_in_days" {
#   type        = number
#   description = "The number of days to retain an application version ('max_age_in_days' and 'max_count' cannot be enabled simultaneously.)"
#   default     = 90
# }

variable "appversion_lifecycle_delete_source_from_s3" {
  type        = bool
  description = "Whether to delete application versions from S3 source"
  default     = false
}

variable "project" {
  type        = string
  description = "Name of project your app belongs to."
  default     = ""
}

variable "environment" {
  type        = string
  description = "Environment where app should be deployed like dev, acc or prd."
  default     = "dev"
}

variable "tags" {
  type        = map(string)
  description = "Additional tags"
  default     = {}
}
